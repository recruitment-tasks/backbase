# Backbase

## UI/UX

* In the project I used the Bootstrap 4 library. I used only the necessary modules.

* I include them here: `Backbase\src\styles.scss`

* I used a set of CSS rules to "reset" styles in browsers. `@import"~bootstrap/scss/reboot";`

* I used a replacement photo from the site: `http: // placeimg.com /`
And I added a unique ID to the URL so that each photo would be different and not be taken from the browser cache.

* For example: `http://placeimg.com/188/140/arch?n= {{city.id}}`

* I added a redirect to a subpage with information about an incorrect URL address.
`Http://localhost:4200/page-not-found`

* I added a spinner for loading data.

* The user can send a page with weather details for the city via messenger.
City id in url.
Example: `http://localhost:4200/detailed-city-weather/3094802`

## Architecture

* I based the data flow on NgRx. I have simplified its use in components as much as possible.
I transferred the rest of the operation to effects, selections and service.
Example: `Backbase\src\app\core\store\app.effects.ts`


* Mapped interfaces with the Open Weather API.
Example: `Backbase\src\app\core\models\openWeatherMap\owmGroupInterface.ts`


* I created a data model for the date list so that the unique key is the city ID.
This allowed to shorten the search time for city data on the subpage up to O(1).
Example: `  3128760: {
               id: 3128760,
               name: 'Barcelona',
               lat: 41.38879,
               lon: 2.15899
             }`
  
             
* For testing, I created a directory with wet data.
Example: `Backbase\src\app\api-mocks`


* I added the Open Weather API key to the environment files to allow it to be quickly changed and dynamically replaced.
Example: `apiKeyForOpenWeatherMap: '78bfdc9a1dfd820ee0fc44bc30fb1c43'`


* Clears weather data when switching between pages.
Example: `  ngOnDestroy(): void {
               this.store.dispatch(cleaningDataForCurrentCity());
             }`

* Tree-shaking.

* OnPush change detection.
            
## Improvements for the future
* Addition of a translation module.

* Adding a graph or developing another component to present weather details for the city.

* Add cache for data download. City Cache: Check the current time. Cache for city list: checking by day.

* Lazy loading modules.

* Micro animations.

* Breadcrumbs.

* Adding SSR and PWA.

* Docker.

* E2E tests.
