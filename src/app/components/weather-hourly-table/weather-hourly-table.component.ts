import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OwmOneCallInterface } from '@core/models/openWeatherMap/owmOneCallInterface';

@Component({
  selector: 'app-weather-hourly-table',
  templateUrl: './weather-hourly-table.component.html',
  styleUrls: ['./weather-hourly-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherHourlyTableComponent {
  @Input() hourly: OwmOneCallInterface['hourly'];
}
