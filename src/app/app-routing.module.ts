import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@core/layout/page-not-found/page-not-found.component';
import { DetailedCityWeatherComponent } from '@core/layout/detailed-city-weather/detailed-city-weather.component';
import { HomePageComponent } from '@core/layout/home-page/home-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'detailed-city-weather/:id', component: DetailedCityWeatherComponent },
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/page-not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
