import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '@env/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from '@core/store/app.effects';
import { backbaseWeatherFeatureKey, backbaseWeatherReducer } from '@core/store/app.reducer';
import { AppComponent } from './app.component';
import { DetailedCityWeatherComponent } from '@core/layout/detailed-city-weather/detailed-city-weather.component';
import { PageNotFoundComponent } from '@core/layout/page-not-found/page-not-found.component';
import { HomePageComponent } from '@core/layout/home-page/home-page.component';
import { HeaderComponent } from '@core/layout/header/header.component';
import { WeatherHourlyTableComponent } from './components/weather-hourly-table/weather-hourly-table.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailedCityWeatherComponent,
    PageNotFoundComponent,
    HomePageComponent,
    HeaderComponent,
    WeatherHourlyTableComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({[backbaseWeatherFeatureKey]: backbaseWeatherReducer}, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: false,
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects]),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
