import { OwmOneCallInterface } from '@core/models/openWeatherMap/owmOneCallInterface';

export const OneCallWarsawMock: OwmOneCallInterface = {
  lat: 52.24,
  lon: 21.02,
  timezone: 'Europe/Warsaw',
  timezone_offset: 7200,
  current: {
    dt: 1597574052,
    sunrise: 1597548091,
    sunset: 1597600743,
    temp: 26.46,
    feels_like: 23.76,
    pressure: 1017,
    humidity: 35,
    dew_point: 9.77,
    uvi: 5.57,
    clouds: 0,
    visibility: 10000,
    wind_speed: 3.82,
    wind_deg: 107,
    weather: [
      {
        id: 800,
        main: 'Clear',
        description: 'clear sky',
        icon: '01d'
      }
    ]
  },
  hourly: [
    {
      dt: 1597572000,
      temp: 26.46,
      feels_like: 23.76,
      pressure: 1017,
      humidity: 35,
      dew_point: 9.77,
      clouds: 0,
      visibility: 10000,
      wind_speed: 3.82,
      wind_deg: 107,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597575600,
      temp: 26.52,
      feels_like: 23.56,
      pressure: 1017,
      humidity: 34,
      dew_point: 9.4,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.05,
      wind_deg: 103,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597579200,
      temp: 27.08,
      feels_like: 23.86,
      pressure: 1017,
      humidity: 32,
      dew_point: 8.98,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.27,
      wind_deg: 100,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597582800,
      temp: 27.33,
      feels_like: 23.96,
      pressure: 1017,
      humidity: 32,
      dew_point: 9.2,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.56,
      wind_deg: 100,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597586400,
      temp: 27.37,
      feels_like: 24.09,
      pressure: 1016,
      humidity: 33,
      dew_point: 9.69,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.62,
      wind_deg: 101,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597590000,
      temp: 26.97,
      feels_like: 23.83,
      pressure: 1016,
      humidity: 35,
      dew_point: 10.36,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.63,
      wind_deg: 102,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597593600,
      temp: 26.3,
      feels_like: 23.45,
      pressure: 1016,
      humidity: 37,
      dew_point: 10.7,
      clouds: 0,
      visibility: 10000,
      wind_speed: 4.3,
      wind_deg: 108,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597597200,
      temp: 25.25,
      feels_like: 22.81,
      pressure: 1016,
      humidity: 40,
      dew_point: 11,
      clouds: 0,
      visibility: 10000,
      wind_speed: 3.81,
      wind_deg: 120,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597600800,
      temp: 23.91,
      feels_like: 21.63,
      pressure: 1016,
      humidity: 42,
      dew_point: 10.31,
      clouds: 0,
      visibility: 10000,
      wind_speed: 3.4,
      wind_deg: 128,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597604400,
      temp: 22.98,
      feels_like: 20.89,
      pressure: 1017,
      humidity: 44,
      dew_point: 10.14,
      clouds: 0,
      visibility: 10000,
      wind_speed: 3.07,
      wind_deg: 129,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597608000,
      temp: 22.07,
      feels_like: 20.06,
      pressure: 1016,
      humidity: 46,
      dew_point: 10.19,
      clouds: 0,
      visibility: 10000,
      wind_speed: 2.9,
      wind_deg: 129,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597611600,
      temp: 21.32,
      feels_like: 19.3,
      pressure: 1016,
      humidity: 47,
      dew_point: 9.7,
      clouds: 0,
      visibility: 10000,
      wind_speed: 2.78,
      wind_deg: 122,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597615200,
      temp: 20.68,
      feels_like: 18.61,
      pressure: 1016,
      humidity: 47,
      dew_point: 9.01,
      clouds: 0,
      visibility: 10000,
      wind_speed: 2.63,
      wind_deg: 122,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597618800,
      temp: 19.98,
      feels_like: 17.77,
      pressure: 1016,
      humidity: 47,
      dew_point: 8.61,
      clouds: 0,
      visibility: 10000,
      wind_speed: 2.6,
      wind_deg: 119,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597622400,
      temp: 19.43,
      feels_like: 17.11,
      pressure: 1015,
      humidity: 48,
      dew_point: 8.41,
      clouds: 0,
      visibility: 10000,
      wind_speed: 2.7,
      wind_deg: 110,
      weather: [
        {
          id: 800,
          main: 'Clear',
          description: 'clear sky',
          icon: '01n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597626000,
      temp: 18.92,
      feels_like: 16.52,
      pressure: 1015,
      humidity: 50,
      dew_point: 8.31,
      clouds: 43,
      visibility: 10000,
      wind_speed: 2.86,
      wind_deg: 103,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597629600,
      temp: 18.39,
      feels_like: 15.87,
      pressure: 1015,
      humidity: 51,
      dew_point: 8.23,
      clouds: 42,
      visibility: 10000,
      wind_speed: 2.96,
      wind_deg: 103,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597633200,
      temp: 18.01,
      feels_like: 15.51,
      pressure: 1015,
      humidity: 53,
      dew_point: 8.51,
      clouds: 28,
      visibility: 10000,
      wind_speed: 3,
      wind_deg: 100,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      pop: 0
    },
    {
      dt: 1597636800,
      temp: 17.93,
      feels_like: 15.49,
      pressure: 1015,
      humidity: 55,
      dew_point: 8.92,
      clouds: 26,
      visibility: 10000,
      wind_speed: 3.09,
      wind_deg: 103,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597640400,
      temp: 18.79,
      feels_like: 16.28,
      pressure: 1015,
      humidity: 55,
      dew_point: 9.78,
      clouds: 33,
      visibility: 10000,
      wind_speed: 3.48,
      wind_deg: 106,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597644000,
      temp: 20.08,
      feels_like: 17.67,
      pressure: 1014,
      humidity: 54,
      dew_point: 10.71,
      clouds: 39,
      visibility: 10000,
      wind_speed: 3.7,
      wind_deg: 105,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597647600,
      temp: 21.92,
      feels_like: 19.73,
      pressure: 1014,
      humidity: 52,
      dew_point: 11.72,
      clouds: 78,
      visibility: 10000,
      wind_speed: 3.85,
      wind_deg: 104,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597651200,
      temp: 24.02,
      feels_like: 21.67,
      pressure: 1014,
      humidity: 48,
      dew_point: 12.5,
      clouds: 70,
      visibility: 10000,
      wind_speed: 4.39,
      wind_deg: 109,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597654800,
      temp: 25.44,
      feels_like: 22.62,
      pressure: 1014,
      humidity: 44,
      dew_point: 12.44,
      clouds: 65,
      visibility: 10000,
      wind_speed: 5.04,
      wind_deg: 110,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597658400,
      temp: 26.77,
      feels_like: 23.9,
      pressure: 1013,
      humidity: 42,
      dew_point: 12.95,
      clouds: 59,
      visibility: 10000,
      wind_speed: 5.33,
      wind_deg: 116,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597662000,
      temp: 26.69,
      feels_like: 24.57,
      pressure: 1013,
      humidity: 45,
      dew_point: 14.02,
      clouds: 65,
      visibility: 10000,
      wind_speed: 4.72,
      wind_deg: 121,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0
    },
    {
      dt: 1597665600,
      temp: 27.75,
      feels_like: 26.42,
      pressure: 1012,
      humidity: 45,
      dew_point: 14.81,
      clouds: 62,
      visibility: 10000,
      wind_speed: 4.07,
      wind_deg: 123,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0.13
    },
    {
      dt: 1597669200,
      temp: 27.85,
      feels_like: 26.25,
      pressure: 1011,
      humidity: 47,
      dew_point: 15.53,
      clouds: 70,
      visibility: 10000,
      wind_speed: 4.84,
      wind_deg: 128,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.41,
      rain: {
        '1h': 0.64
      }
    },
    {
      dt: 1597672800,
      temp: 27.6,
      feels_like: 26.09,
      pressure: 1011,
      humidity: 47,
      dew_point: 15.58,
      clouds: 72,
      visibility: 10000,
      wind_speed: 4.6,
      wind_deg: 134,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.69,
      rain: {
        '1h': 0.32
      }
    },
    {
      dt: 1597676400,
      temp: 27.4,
      feels_like: 26.1,
      pressure: 1011,
      humidity: 48,
      dew_point: 15.67,
      clouds: 76,
      visibility: 10000,
      wind_speed: 4.37,
      wind_deg: 130,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.84,
      rain: {
        '1h': 0.11
      }
    },
    {
      dt: 1597680000,
      temp: 26.85,
      feels_like: 25.7,
      pressure: 1010,
      humidity: 51,
      dew_point: 15.89,
      clouds: 74,
      visibility: 10000,
      wind_speed: 4.4,
      wind_deg: 133,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.88,
      rain: {
        '1h': 0.18
      }
    },
    {
      dt: 1597683600,
      temp: 25.87,
      feels_like: 25,
      pressure: 1010,
      humidity: 54,
      dew_point: 16.06,
      clouds: 59,
      visibility: 10000,
      wind_speed: 4,
      wind_deg: 131,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04d'
        }
      ],
      pop: 0.75
    },
    {
      dt: 1597687200,
      temp: 24.47,
      feels_like: 23.61,
      pressure: 1010,
      humidity: 60,
      dew_point: 16.26,
      clouds: 49,
      visibility: 10000,
      wind_speed: 4.17,
      wind_deg: 131,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n'
        }
      ],
      pop: 0.99,
      rain: {
        '1h': 0.21
      }
    },
    {
      dt: 1597690800,
      temp: 23.58,
      feels_like: 22.71,
      pressure: 1010,
      humidity: 63,
      dew_point: 16.25,
      clouds: 9,
      visibility: 10000,
      wind_speed: 4.15,
      wind_deg: 130,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n'
        }
      ],
      pop: 0.72,
      rain: {
        '1h': 0.32
      }
    },
    {
      dt: 1597694400,
      temp: 22.58,
      feels_like: 22.06,
      pressure: 1010,
      humidity: 67,
      dew_point: 16.33,
      clouds: 17,
      visibility: 10000,
      wind_speed: 3.65,
      wind_deg: 131,
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10n'
        }
      ],
      pop: 0.96,
      rain: {
        '1h': 1.14
      }
    },
    {
      dt: 1597698000,
      temp: 22.13,
      feels_like: 22.1,
      pressure: 1010,
      humidity: 69,
      dew_point: 16.4,
      clouds: 33,
      visibility: 10000,
      wind_speed: 2.98,
      wind_deg: 121,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n'
        }
      ],
      pop: 1,
      rain: {
        '1h': 0.85
      }
    },
    {
      dt: 1597701600,
      temp: 21.42,
      feels_like: 21.44,
      pressure: 1010,
      humidity: 73,
      dew_point: 16.59,
      clouds: 26,
      visibility: 10000,
      wind_speed: 3.01,
      wind_deg: 124,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n'
        }
      ],
      pop: 1,
      rain: {
        '1h': 0.63
      }
    },
    {
      dt: 1597705200,
      temp: 20.98,
      feels_like: 21.22,
      pressure: 1010,
      humidity: 77,
      dew_point: 16.88,
      clouds: 27,
      visibility: 10000,
      wind_speed: 2.94,
      wind_deg: 137,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10n'
        }
      ],
      pop: 1,
      rain: {
        '1h': 0.18
      }
    },
    {
      dt: 1597708800,
      temp: 20.77,
      feels_like: 21.3,
      pressure: 1009,
      humidity: 79,
      dew_point: 17.16,
      clouds: 39,
      visibility: 10000,
      wind_speed: 2.64,
      wind_deg: 141,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      pop: 0.8
    },
    {
      dt: 1597712400,
      temp: 20.57,
      feels_like: 21.39,
      pressure: 1009,
      humidity: 81,
      dew_point: 17.24,
      clouds: 58,
      visibility: 10000,
      wind_speed: 2.34,
      wind_deg: 153,
      weather: [
        {
          id: 803,
          main: 'Clouds',
          description: 'broken clouds',
          icon: '04n'
        }
      ],
      pop: 0.15
    },
    {
      dt: 1597716000,
      temp: 20.28,
      feels_like: 21.22,
      pressure: 1009,
      humidity: 82,
      dew_point: 17.23,
      clouds: 29,
      visibility: 10000,
      wind_speed: 2.12,
      wind_deg: 156,
      weather: [
        {
          id: 802,
          main: 'Clouds',
          description: 'scattered clouds',
          icon: '03n'
        }
      ],
      pop: 0.26
    },
    {
      dt: 1597719600,
      temp: 20.02,
      feels_like: 21.19,
      pressure: 1008,
      humidity: 82,
      dew_point: 17.01,
      clouds: 19,
      visibility: 10000,
      wind_speed: 1.64,
      wind_deg: 156,
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02n'
        }
      ],
      pop: 0.42
    },
    {
      dt: 1597723200,
      temp: 19.89,
      feels_like: 21.18,
      pressure: 1008,
      humidity: 83,
      dew_point: 16.93,
      clouds: 17,
      visibility: 10000,
      wind_speed: 1.5,
      wind_deg: 151,
      weather: [
        {
          id: 801,
          main: 'Clouds',
          description: 'few clouds',
          icon: '02d'
        }
      ],
      pop: 0.47
    },
    {
      dt: 1597726800,
      temp: 20.58,
      feels_like: 21.59,
      pressure: 1008,
      humidity: 79,
      dew_point: 16.82,
      clouds: 35,
      visibility: 10000,
      wind_speed: 1.84,
      wind_deg: 162,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.55,
      rain: {
        '1h': 0.18
      }
    },
    {
      dt: 1597730400,
      temp: 21.61,
      feels_like: 22.43,
      pressure: 1008,
      humidity: 74,
      dew_point: 16.96,
      clouds: 46,
      visibility: 10000,
      wind_speed: 2.09,
      wind_deg: 176,
      weather: [
        {
          id: 500,
          main: 'Rain',
          description: 'light rain',
          icon: '10d'
        }
      ],
      pop: 0.62,
      rain: {
        '1h': 0.52
      }
    },
    {
      dt: 1597734000,
      temp: 22.77,
      feels_like: 23.55,
      pressure: 1008,
      humidity: 69,
      dew_point: 16.97,
      clouds: 100,
      visibility: 10000,
      wind_speed: 2.16,
      wind_deg: 196,
      weather: [
        {
          id: 804,
          main: 'Clouds',
          description: 'overcast clouds',
          icon: '04d'
        }
      ],
      pop: 0.32
    },
    {
      dt: 1597737600,
      temp: 24.4,
      feels_like: 25.23,
      pressure: 1008,
      humidity: 63,
      dew_point: 16.96,
      clouds: 100,
      visibility: 10000,
      wind_speed: 2.15,
      wind_deg: 211,
      weather: [
        {
          id: 804,
          main: 'Clouds',
          description: 'overcast clouds',
          icon: '04d'
        }
      ],
      pop: 0.16
    },
    {
      dt: 1597741200,
      temp: 25.99,
      feels_like: 26.33,
      pressure: 1007,
      humidity: 56,
      dew_point: 16.56,
      clouds: 100,
      visibility: 10000,
      wind_speed: 2.64,
      wind_deg: 201,
      weather: [
        {
          id: 804,
          main: 'Clouds',
          description: 'overcast clouds',
          icon: '04d'
        }
      ],
      pop: 0.23
    }
  ]
};
