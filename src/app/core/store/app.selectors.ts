import { createFeatureSelector, createSelector } from '@ngrx/store';
import { backbaseWeatherFeatureKey, BackbaseWeatherState } from './app.reducer';
import { City } from '@core/models/app';

export const selectBackbaseWeatherState = createFeatureSelector<BackbaseWeatherState>(
  backbaseWeatherFeatureKey
);

export const listOfCitiesSelector = createSelector(selectBackbaseWeatherState, (state: BackbaseWeatherState) => {
  return state.initListOfCities;
});

export const getCityByIdSelector = createSelector(
  selectBackbaseWeatherState,
  (state: BackbaseWeatherState, cityId: City['id'] ): City => {
  return state.initListOfCities.hasOwnProperty(cityId) && state.initListOfCities[cityId];
});

export const getCurrentCitySelector = createSelector(
  selectBackbaseWeatherState,
  (state: BackbaseWeatherState) => {
    return state.currentCity;
  });

export const getCityWeatherDataListSelector = createSelector(
  selectBackbaseWeatherState,
  (state: BackbaseWeatherState) => {
    return state.cityWeatherDataList;
  });
