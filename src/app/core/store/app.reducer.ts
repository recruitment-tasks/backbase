import { createReducer, on } from '@ngrx/store';
import {City, InitListOfCities} from '@core/models/app';
import {
  cleaningDataForCurrentCity,
  getWeatherDataForAllCitiesSuccessAction,
  getWeatherDataForCitySuccessAction
} from '@core/store/app.actions';
import {OwmOneCallInterface} from '@core/models/openWeatherMap/owmOneCallInterface';
import {OwmGroupInterface} from '@core/models/openWeatherMap/owmGroupInterface';

export const backbaseWeatherFeatureKey = 'BackbaseWeather';

export interface BackbaseWeatherState {
  currentCity: OwmOneCallInterface | null;
  initListOfCities: {[id: number]: City};
  cityWeatherDataList: OwmGroupInterface['list'] | null;
}

export const initialBackbaseWeatherState: BackbaseWeatherState = {
  currentCity: null,
  initListOfCities: InitListOfCities,
  cityWeatherDataList: null
};

export const backbaseWeatherReducer = createReducer(
  initialBackbaseWeatherState,
  on(getWeatherDataForCitySuccessAction,
    (state, action) => ({...state, currentCity: action.data.currentCity}),
  ),
  on(getWeatherDataForAllCitiesSuccessAction,
    (state, action) => ({...state, cityWeatherDataList: action.data.cityWeatherDataList})
  ),
  on(cleaningDataForCurrentCity,
    (state) => ({...state, currentCity: initialBackbaseWeatherState.currentCity})
  )
);

