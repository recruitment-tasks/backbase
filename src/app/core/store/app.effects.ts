import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import {
  getWeatherDataForAllCitiesFailureAction,
  getWeatherDataForAllCitiesAction,
  getWeatherDataForAllCitiesSuccessAction,
  getWeatherDataForCityAction,
  getWeatherDataForCityFailureAction,
  getWeatherDataForCitySuccessAction
} from '@core/store/app.actions';
import { WeatherService } from '@core/services/weather.service';
import { BackbaseWeatherState } from '@core/store/app.reducer';
import { getCityByIdSelector, listOfCitiesSelector } from '@core/store/app.selectors';
import { City } from '@core/models/app';
import { OwmOneCallInterface } from '@core/models/openWeatherMap/owmOneCallInterface';
import { OwmGroupInterface } from '@core/models/openWeatherMap/owmGroupInterface';

@Injectable()
export class AppEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<BackbaseWeatherState>,
    private weatherService: WeatherService
  ) {
   }

  getWeatherDataForCityEffect$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(getWeatherDataForCityAction),
        map(action => action.data.cityId),
        mergeMap(
          (cityId: City['id']) =>
            of(cityId).pipe(
              withLatestFrom(
                this.store$.pipe(select(state => getCityByIdSelector(state, cityId)))
              )
            )
        ),
        switchMap(
          ([, cityData]) => this.weatherService.getWeatherDataForCity(cityData)
            .pipe(
              map( (city: OwmOneCallInterface) => getWeatherDataForCitySuccessAction({data: {currentCity: city}})),
              catchError(() => of(getWeatherDataForCityFailureAction))
            )
        )
      );
    }
  );

  getWeatherDataForAllCitiesEffect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(getWeatherDataForAllCitiesAction),
      withLatestFrom(this.store$.pipe(
        select(
          listOfCitiesSelector
        )
      )),
      switchMap(
        ([, cities]) => this.weatherService.getWeatherDataForAllCities(cities)
          .pipe(
            map(
              (citiesList: OwmGroupInterface['list']) =>
                getWeatherDataForAllCitiesSuccessAction({data: {cityWeatherDataList: citiesList}})
            ),
            catchError(() => of(getWeatherDataForAllCitiesFailureAction))
          )
      )
    );
  });

}
