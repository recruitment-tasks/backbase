import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable} from 'rxjs';
import { BackbaseWeatherState } from '@core/store/app.reducer';
import { getCityWeatherDataListSelector } from '@core/store/app.selectors';
import { getWeatherDataForAllCitiesAction } from '@core/store/app.actions';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit {
  cityWeatherDataList$: Observable<BackbaseWeatherState['cityWeatherDataList']>;

  constructor(
    private store: Store<BackbaseWeatherState>
  ) {
  }

  ngOnInit(): void {
    this.loadWeatherDataForListOfCities();
  }

  private loadWeatherDataForListOfCities(): void {
    this.store.dispatch(getWeatherDataForAllCitiesAction());
    this.cityWeatherDataList$ = this.store.pipe(select(getCityWeatherDataListSelector));
  }
}
