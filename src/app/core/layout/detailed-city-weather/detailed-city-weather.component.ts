import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { City } from '@core/models/app';
import { cleaningDataForCurrentCity, getWeatherDataForCityAction } from '@core/store/app.actions';
import { BackbaseWeatherState } from '@core/store/app.reducer';
import { getCityByIdSelector, getCurrentCitySelector } from '@core/store/app.selectors';

@Component({
  selector: 'app-detailed-city-weather',
  templateUrl: './detailed-city-weather.component.html',
  styleUrls: ['./detailed-city-weather.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailedCityWeatherComponent implements OnInit, OnDestroy {
  currentCity$: Observable<BackbaseWeatherState['currentCity']>;
  cityData$: Observable<City>;

  constructor(
    private route: ActivatedRoute,
    private store: Store<BackbaseWeatherState>,
  ) { }

  ngOnInit(): void {
    const cityId: number = this.getCityIdFromUrlParam();

    this.loadWeatherDataForCity(cityId);
    this.loadWeatherDataForCityFromStaticData(cityId);
  }

  ngOnDestroy(): void {
    this.store.dispatch(cleaningDataForCurrentCity());
  }

  private getCityIdFromUrlParam(): number {
    return Number(this.route.snapshot.paramMap.get('id'));
  }

  private loadWeatherDataForCity(cityId: City['id']): void {
    this.store.dispatch(getWeatherDataForCityAction({ data: { cityId } }));

    this.currentCity$ = this.store.pipe(
      select(getCurrentCitySelector),
      filter((currentCity: BackbaseWeatherState['currentCity'])  => currentCity !== null)
    );
  }

  private loadWeatherDataForCityFromStaticData(cityId: City['id']): void {
    this.cityData$ = this.store.pipe(
      select( state => getCityByIdSelector(state, cityId)),
      filter((cityData: City)  => cityData !== null)
    );
  }
}
