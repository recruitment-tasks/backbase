import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponent } from 'ng-mocks';
import { getCurrentCitySelector } from '@core/store/app.selectors';
import { DetailedCityWeatherComponent } from './detailed-city-weather.component';
import { WeatherHourlyTableComponent } from '../../../components/weather-hourly-table/weather-hourly-table.component';

describe('DetailedCityWeatherComponent', () => {
  let component: DetailedCityWeatherComponent;
  let fixture: ComponentFixture<DetailedCityWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DetailedCityWeatherComponent,
        MockComponent(WeatherHourlyTableComponent)
      ],
      providers: [
        provideMockStore({
          selectors: [
            { selector: getCurrentCitySelector, value: '' }
          ]
        }),
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({id: 1})
            }
          },
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedCityWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => { fixture.destroy(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
