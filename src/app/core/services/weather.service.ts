import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { City } from '../models/app';
import { OwmOneCallInterface } from '@core/models/openWeatherMap/owmOneCallInterface';
import { OwmGroupInterface } from '@core/models/openWeatherMap/owmGroupInterface';
import { BackbaseWeatherState } from '@core/store/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private apiKey = environment.apiKeyForOpenWeatherMap;
  private domain = 'http://api.openweathermap.org';
  private apiVersion = '2.5';
  private prefixApiUrl = `${this.domain}/data/${this.apiVersion}`;
  private commonHttpParamsToString = '';

  private oneCallApiUrl = `${this.prefixApiUrl}/onecall`;
  private groupApiUrl = `${this.prefixApiUrl}/group`;

  constructor(
    protected httpClient: HttpClient
  ) {
    this.prepareCommonHttpParams();
  }

  private prepareCommonHttpParams(): void {
    this.commonHttpParamsToString = new HttpParams()
      .set('APPID', this.apiKey)
      .set('units', 'metric')
      .toString();
  }

  getWeatherDataForCity(city: City): Observable<OwmOneCallInterface> {
    const httpParams = new HttpParams({ fromString: this.commonHttpParamsToString })
      .set('lat', String(city.lat))
      .set('lon', String(city.lon));

    return this.httpClient.get<OwmOneCallInterface>(this.oneCallApiUrl, { params: httpParams });
  }

  getWeatherDataForAllCities(cities: BackbaseWeatherState['initListOfCities']): Observable<OwmGroupInterface['list']> {
    const cityIdsToString = Object.keys(cities).join(',');
    const httpParams = new HttpParams({ fromString: this.commonHttpParamsToString })
      .set('exclude', 'daily,minutely')
      .set('id', cityIdsToString);

    return this.httpClient
      .get<OwmGroupInterface>(this.groupApiUrl, { params: httpParams })
      .pipe(
        map( (group: OwmGroupInterface ) => group.list )
      );
  }
}
